FROM php:7.3-apache

RUN apt-get clean
RUN apt-get update
RUN DEBIAN_FRONTEND=noninteractive apt-get install -yq \
        git \
        tree \
        vim \
        wget \
        nano \
        iperf3

COPY workload_job.php /var/www/html/index.php
RUN chmod a+rx index.php
RUN export JOB_LOOP=1000000
RUN export JOB_VAR=0.0002
